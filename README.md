## Marketing Operations Team  

- [Claudia Beer](https://gitlab.com/cbeer) - Manager, Marketing Operations
- [Beth Peterson](https://gitlab.com/bethpeterson) - Senior Marketing Operations Manager
- [Amy Waller](https://gitlab.com/amy.waller) - Senior Marketing Operations Manager
- [Sarah Daily](https://gitlab.com/sdaily) - Senior Marketing Operations Manager
- [Robert Rosu](https://gitlab.com/RobRosu) - Marketing Operations Manager 
- [Gillian Murphy](https://gitlab.com/gillmurphy) - Marketing Operations Manager
- [Jameson Burton](https://gitlab.com/jburton) - Marketing Operations Associate

**Business Partners**

- SDRs - Beth Peterson
- Campaigns - Amy Waller
- Content - Sarah Daily

## How to Communicate with Us

**Slack channels**

We do not use or create tool-specific Slack channels (e.g. `#marketo`).

- [#mktgops](https://gitlab.slack.com/archives/mktgops) - We use this channel for general marketing operations support, weekly marketing operations team standup updates, and key system status updates. We attempt to [avoid direct messages](https://about.gitlab.com/handbook/communication/#avoid-direct-messages) where possible as it discourages collaboration. 
- [#hbupdate-mktgops](https://gitlab.slack.com/archives/mktgops) - This channel is used to automatically post new [handbook updates](#handbook-updates) that have been merged. 

## Working with Marketing Operations  

**Our Motto:** `If it isn't an **issue**, it isn't OUR issue!`

[Create a new issue for marketing operations](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new)

### Labels to use

#### GitLab Team Members

- `MktgOps - FYI`: Issue is not directly related to operations, no action items for MktgOps but need to be aware of the issue
- `MktgOps::0 - To Be Triaged`: Issue initially created, used in templates, the starting point for any label that involves MktgOps (except for List Uploads); no real discussion on the issue; generally unassigned
- `MktgOps - List Import`: Used for list imports of any kind - event or general/ad hoc (do not also use To Be Triaged scoped label)
- `MktgOps - changelog`: Used to track issues or epics that would need to be logged in the marketing changelog to track major changes across marketing
- `MktgOps - Future Feature`: Something to consider for a future project as time allows. Timeframe: As time allows

#### Marketing Ops Only

**Priority**

- `MktgOps-Priority::Top Priority`: Issue that is related to a breaking change, OKR focus, any other prioritized project by MktgOps leadership. This category will be limited because not everything can be a priority. Timeframe: Immediate action needed.
- `MktgOps-Priority:: High Priority`: Issue has a specific action item for MktgOps to be completed as it is an OKR. Timeframe: Within weeks
- `MktgOps-Priority::Med Priority`: Issue is a feature to help the team. Timeframe: Within months
- `MktgOps-Priority::Low Priority`: Issue has a specific action item for MktgOps that would be helpful, but can be pushed for other issues. Timeframe: Within months.

**Urgency**

- `MktgOps-Urgency::P0`: System down, core business function down, or potential loss of mission critical data. Timeframe: Immediate action needed. Pull into active milestone.
- `MktgOps-Urgency::P1`: Major feature or workflow is not working. Timeframe: Action within days. Pull into active milestone.
- `MktgOps-Urgency::P2`: Requires attention, but normal workflow is not impacted or there is a workaround. Timeframe: Action within next milestone.
- `MktgOps-Urgency::P3`: Requires attention, but normal workflow is not impacted. Minor, low urgency. Timeframe: Future action needed, not priority or urgent.
- `MktgOps-Urgency::P4`: Small bug, nice to have fixed, but not impacting workflow. Timeframe: Future action needed, not priority or urgent.

**Stage**

- `MktgOps::1 - Planning`: Issues assigned to a MktgOps team member and are currently being planned but are not being actively worked on yet.
- `MktgOps::2 - Scoping`: Marketing Operations related issues that are currently being scoped and weighted
- `MktgOps::3 - On Deck`: Issues that have been scoped and will be added to an upcoming milestone.
- `MktgOps::4 - In Process`: Issues that are actively being worked on in the current two-week milestone.
- `MktgOps::5 - UAT`: Issues that MktgOps has completed its required tasks for and is ready for User Acceptance Testing/review and approval by the Requester/Approver.
- `MktgOps::6 - On Hold`: Issue that is not within existing scope of MktgOps current focus, or another department as deprioritized. May be a precursor to being closed out. May also be on hold waiting for an internal team member or other task before completing. 
- `MktgOps::7 - Blocked`: Issue is blocked and no other actions can be taken by MktgOps. Waiting for someone else/another team to complete an action item before being able to proceed. May also be blocked due to external party such as a vendor to complete an action before closing.
- `MktgOps::8 - Completed`: MktgOps has completed their task on this issue although the issue may not be closed.

**Other**

- `MktgOps - bug`: A bug issue to be addressed by Marketing Operations
- `MktgReporting-FYI`: Awareness for Marketing Strategy & Performance team related to reporting
