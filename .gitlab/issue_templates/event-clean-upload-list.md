### 📝 [Epic Link >>](#)

## Purpose

This issue will track the list cleanup and upload for events. Adherence to timelines and receiving lists as soon as possible from event organizers is critical to a timely follow up. **For further directions from the handbook, please refer to the [List Upload page](https://about.gitlab.com/handbook/marketing/marketing-operations/list-import/).**

**Upload instructions to be completed by MktgOPS, organized by Marketing Programs and with support of Field Marketing as liaison with event organizers.**

### :pushpin: Required Fields for Successful List Upload   
- `First Name`
- `Last Name`
- `Company`
- **Origin** - This is where you recieved the list from, in EMEA only GDPR compliant sources are allowed to be imported and prospected to
- `Email` - records without an email address **will not** be imported  
- Address fields  - `Street`, `City`, `State`, etc need to be in their own individual column
- `Opt-in` - Only mark as TRUE if the prospects specifically opted into email communication. You must provide a screenshot of the language used in the issue comments.

## 🛁 List clean step 1 (Campaign owner @)

* [ ] Select File -> Make Copy of [full spreadsheet](https://docs.google.com/spreadsheets/d/143REaMQLyIy7to-CFktL45TTTLZxBQRJUDIOMCA3CVo/edit#gid=257616838) into a new spreadsheet. Use template to copy/paste columns and alert to any email syntax errors, duplicates, and clean up Proper Capitalization. 
     - [ ] Remove duplicates based on template alerts and correct errors in emails, as displayed
     - [ ] Remove any `n/a`, `self`, etc from `Title`
     - [ ] Remove any `State` values for Countries that are **not** `United States` or `Canada`
     - [ ] Ensure all `Phone` values are actual phone numbers
* [ ] Work with event booth staff to clean and take notes on conversations
* [ ] Translate international lead lists as necessary - campaign owner to delegate, if relevant
* [ ] Once cleaning is completed, take cleaned data from `Right/Green` side of template and `Paste values` into the `Left/Blue` side of spreadsheet. 
 `Paste values` will paste **only data** and remove present formulas
* [ ] After cleaned data is moved to `Left/Blue` side of spreadsheet, fully delete right/green side of spreadsheet
* [ ] Rename the file to match the relevant `campaign tag`, if applicable, or campaign name 
* [ ] Link to ready-to-upload googledoc of list in issue comments, tag MktgOps (primarily @jburton)
* [ ] Add ~"List Upload: Ready" label to issue. **IMPORTANT: Reapply label for each new upload request** 
* [ ] [Change ownership of list spreadsheet](https://support.google.com/docs/answer/2494892?co=GENIE.Platform%3DDesktop&hl=en) to Marketing Ops rep performing the upload so they may erase the spreadsheet. Keep editing access for yourself
* [ ] If you have an `Add to Nurture` issue request, please ping the Campaign Manager in that issue once your list upload is confirmed.

## 🔼 Upload list to Salesforce & notify sales development (MktgOPS)

* [ ] Upload list to Marketo
* [ ] Check for appropriate changes to `Initial Source`, `Person Status`, `Acquisition Program` and `Campaign Member Status`
* [ ] Notify in issue when list is uploaded
* [ ] Post message in `event_list_upload` Slack channel
* [ ] Remove ~"List Upload: Ready" label
* [ ] Apply label `Lead Data: Active` if it is not already attached
* [ ] Return later to remove lead data from G Suite and switch label to `Lead Data: Inactive`

## :bulb: Add to Nurture for Field Marketing (FMC @)

* [ ] Once lead list upload has been completed, FMC to triage [Add to Nurture](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/request_add_nurture.md) issue. Leads must be uploaded before nurture issue can be completed.

### :clock: List imports will be completed within 24 HOURS after receiving the list from Field Marketing.  

Please submit any questions about this template to the #mktgops slack channel.

/label ~"mktg-status::plan" ~"MktgOps - List Import"
/confidential
