## User story

<!-- Please submit issue with the following format: As a _____ (role in marketing), I would like to ____________(need), so that ________________(reason).-->
<!-- Example: As GTM management, I would like to know that all Trials are MQLing, so that MQL metrics are accurate and we can rely on them for decision making.-->

## ✔️ Check the appropriate request below:

* [ ]  New shared search in PathFactory for Sales
* [ ]  New form request ([must have an existing Marketo form first](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#pathfactory-forms))
* [ ]  New theme creation
* [ ]  New CTA
* [ ]  New content tag (must be approved by content team)
* [ ]  New content type (must be approved by content team)
* [ ]  Other (please describe below)

## 📝 Please fill out the description information for the request:





Please submit any questions about this template to the #mktgops slack channel.

<!-- Please leave the label below on this issue -->

/label ~"MktgOps::0 - To Be Triaged" ~"PathFactory" ~"mktg-status::plan"
/assign @sdaily