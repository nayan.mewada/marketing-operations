Please use this issue to request MktgOps assistance in creating a custom Drift conversational landing page, bot experience or welcome message for a marketing campaing, event, etc. For more information about Drift at GitLab check out this [handbook page](handbook/marketing/marketing-operations/drift/). 

### Instructions
1. Please title issue using the following format: Drift for [Campaign/Event Name] - [Team Name] Team
1. Fill out the description section below.
1. Add a due date to this issue that corresponds with the day you need the change to be effective. Please make an effort to open this issue at least seven business days before a change needs to be implemented. 

### Description
Please briefly descirbe what you are looking to leverage Drift for and fill out the rest of the section.

**Need:**
* [ ] Conversational landing page
* [ ] Bot experience/messaging
* [ ] Welcome message

- **Target audience/account list:**
- **Overall theme/content description:**
- **Assets to include:**
- **Helpful resources:**


/label ~"MktgOps::0 - To Be Triaged" ~Drift 

/assign @bethpeterson

