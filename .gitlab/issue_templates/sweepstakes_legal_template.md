<!-- Title: Request for legal approval: [Event name, type of event] -->

### 📝 [Epic Link >>](#)

## Purpose

This issue will serve as the location for marketing team members to receive legal feedback and approval for upcoming sweepstakes. 

### Standards for any GitLab sweepstakes

For any sweepstakes, please review the [Collaborating with GitLab Legal](https://about.gitlab.com/handbook/legal/marketing-collaboration/#contests-and-sweepstakes) handbook page. If there are changes needed, Legal will provide updated terms for your event. Please note that the legal team may approve or deny these changes based upon local law in the participating countries. 

## Please answer the following questions about your intended sweepstakes

What type of event are you planning:
- [ ] Survey
- [ ] Quiz
- [ ] Skill Contest

---
**Event General Details**
1. When will the event take place and what is the duration? 
<!-- Provide the dates and times (including time zone) that the event will start and end. Please add a due date to the issue side bar if you need approval more quickly than 2 weeks. -->

1. Is this related to a campaign, conference, webinar, etc? If so, which one(s)?

1. In what country or countries will the event be hosted? 

1. Please provide the event description intended to be used for public display.

1. What method of advertising is to be used for the event?
<!-- Provide a link to the url for the event. -->

1. What do you intend to do with the information gained and how are you informing the entrants of this?

1. Please provide the questions being used in the survey/quiz.

---

**Participant Information**

1. Detail who is allowed to enter, as well as those who are excluded.

1. How will entrants sign up? 

1. What are the registration requirements? (e.g. attend x webinar; no purchase necessary, etc.)

1. What residential status is required for participants, if any? (e.g. resident of EMEA countries)

---
**After the Event**

1. Is there an intention to use the event for marketing outreach? If so, how will the entrant opt out of marketing emails?

---
**Prizes**

1. What prizes are involved? 
<!-- Detail the exact prizes and the value of the prizes. If more than one prize will be distributed, be sure to specify how many of each prize will be available. Include a link to the prize, if possible. -->

1. Detail how and when a winner will be chosen for the prize.
<!-- If the award will be based on skill, please note who the judges will be and the specific criteria that will be evaluated. -->

1. How and when will the winner be notified?

1. Where will the winner list be posted?
<!-- Please provide the url -->

---
---
**T&Cs**
_Note: Legal reviewer will post WIP T&Cs here_

<!-- Please leave the label below on this issue -->
/label ~"Mktg Promo::Pending_Legal_Review" ~"Compliance Guidance" ~"Marketing Programs" ~"Field Marketing"

/confidential
/due in 2 weeks
