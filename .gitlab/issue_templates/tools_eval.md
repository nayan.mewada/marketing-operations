[DRI](https://about.gitlab.com/handbook/people-operations/directly-responsible-individuals/) to fill in as much as possible and assign yourself as owner initially. Please ensure the interested parties are part of the business needs evaluation.

## Step 1 Functional approval from your dept (required)

- [ ] Add link to documentation from your management approving the investigation of this tool. The approval depth should mimmic the [GitLab Authorization matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix) from rounds of approval. As an example, if the cost of this tool seeks approval up to the exec team, then you must have your exec team approval in order for ops to begin their evaluation. 

## Step 2 Business Needs Intake (Required)

- Business Problem Statement: `2-3 sentences on the business problem`
- Solution Statement: `2-3 sentences on how this purchase will solve the problem`
- Description of Purchase: `2-3 sentences on what we plan to buy and what we will get (i.e. implementation professional services for the design, build, and rollout of new tool for 3 months...)`
- Identify software need (check one):
  - [ ] New functionality in our [tech stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0)
  - [ ] Replaces a tool we currently have application to be replaced `renewal date`
  - [ ] Renewal for a tool we currently have application
  - [ ] Additional licenses for a tool we currently have application*
- Business Justification: 2-3 sentences on spend justification
  - [ ] Elimination of existing costs (i.e. software, expenses, etc) `enter savings`
  - [ ] Headcount savings `enter savings`
  - [ ] Enabled time savings for team (i.e. increased productivity) `enter savings`
  - [ ] Enabled capability/services `enter savings justification`
- Budget holder and Tool Eval approvals
  - [ ] Budget holder: `assign budget holder director`
  - [ ] Tool Eval: `assign department director`

### Status of existing tool

If you selected `Replaces a tool we currently have` above, please fill out the additional details below:

- Do we currently have a tool in use for this function?
- If we do, why are we replacing it?
- What is the current contractual status? Link to copy of contract:

## Step 3 Vendor Needs Intake (Required)

- Vendor: `Vendor Name and company URL`
- Application: `vendor application name`
- Anticipated usage for next 12 months: `estimated user count/bandwidth with ramp schedule`
- Vendor contact: `vendor contact name and email for quote and/or contract signatures`
- Vendor billing contact: `vendor billing contact name & email for payment (this is not the same as the sales rep)`
- Attach unsigned contract/quotes if received: `attach here`
  - [ ] If this is a renewal, attach previous order form: `here`
- Total Contract Value: `indicate total contract value, estimate if unknown`
  - [ ] If spend is >$100K, attach competitive quote here
  - [ ] If spend is >$250K, execute RFP link RFP issue here
- Does this application integrate with our enterprise ecosystem? (Salesforce, Zuora, Marketo, Customer portal, etc) `yes-which ones? or no`
- What data is involved? Describe category of data (e.g. Sales, Customer Usage), specific fields of data, volume, etc. `here`
- Will there be a contractor involved who will need to access our systems or non-public data? `yes or no`
- Will you be reporting on data from this application from our warehouse or Business Intelligence tool? `yes or no`
  - [ ] If so, please submit a [discovery issue for the Data Team](https://gitlab.com/gitlab-data/analytics/issues/new?issuable_template=Extraction%20Discovery)

## Evaluation process

- [ ]  Vendor pitch attended by:
- [ ]  Buy-in from interested parties @ mention:
- [ ]  Demo
- [ ]  Any hands-on use
- [ ]  Marketing Ops business partner included in evaluation: 
- [ ]  If approved, [submit a finance issue](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/issues/new?issuable_template=software_vendor_contract_request) **at least a month before purchase** - [Purchase request process](https://about.gitlab.com/handbook/finance/procurement/purchase-request-process/)

## Documentation 

- [ ] Add links to related documentation under this section as you collect them (Request for proposal, user stories, requirements, etc.):

/label ~"MktgOps::0 - To Be Triaged" ~"mktg-status::plan"
