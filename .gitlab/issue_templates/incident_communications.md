NOTE: This is for urgent and emergency communications only, for other email requests, please use a different [email issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new#request-email) template.


This is the process to follow when there is an announcement that is an emergency that will need to be sent by the marketing ops and campaigns team. The marketing teams will determine what platform will be used based on the information that is provided to them, such as timing, list size and severity. Any information you provide will help the team determine the best course of action.

:exclamation: In addition to submitting the details below, please share issue link in [#mktgops slack channel](https://app.slack.com/client/T02592416/CGL35F20G)and tag @awaller and @nout. If there is not a prompt response from a team member, [follow on-call procedures](https://about.gitlab.com/handbook/marketing/marketing-operations/#pagerduty).

Please create an issue for **EVERY** email that you need to send.

# Type of Incident
- [ ] Security
- [ ] Other ____


# Purpose and description
<!--Please include relevant details and links about the incident you need to communicate about, relevant slack channels and issues are helpful-->


## Submitter Checklist
* [ ] Name this issue `URGENT: Email Request: <brief description of email purpose>` (ex. Email Request: notify SaaS users of Incident #123)

* **Provide logistical details:**
   - [ ] Goal of this email: `please be as specific as possible`
   - [ ] Requested Send Date: /due <!--Insert date, or today if ASAP-->
   - [ ] Requested Send Time <!--add specific time & timezone)-->
   - [ ] Recipient List: <!--link to target list issue--> 
   - [ ] Anticipated Audience Size:  <!--Best guess here is OK, over estimate or give a range--> 
   - [ ] Reviewers/Approvers: <!--include email address of reviewers-->

* **Provide email setup details:**
   - [ ] From Name: `default: GitLab Team`
   - [ ] From Email: `default: info@gitlab.com`
   - [ ] Respond-to Email: `default: info@gitlab.com`

 * **Provide email setup details:**
     * [ ] Will there be any fields that will be merged in the copy? (Ex. Project Names)
           - [ ] If so, please list them and character requirements
     * [ ] **Provide Final Copy:** <!--add hyperlink to final copy googledoc--> [please use approved copy doc](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit#heading=h.6vgpxnyb4dh1)
        - Note: We understand copy may be a WIP, but please give us access early on so we can anticipate needs.
        - Note: please check that doc is editable by check that doc is editable by selecting `Anyone with this link can edit` (enabling our email markeitng agency)
        - When copy is final, please note in this issue.


## Reviewer Checklist
* [ ] Triage Manager: Confirm that required details were provided and issue is ready to be worked, ask questions as necessary
* [ ] Discuss the proposed send day and time, ask questions as necessary
* [ ] Determine best platform for send
* [ ] Load in list for send
* [ ] Create smart campaign for processing and to avoid routing
* [ ] Email Validation (if necessary)
* [ ] Create the email
* [ ] Send test email to the requester and designated additional reviewer/approvers in the "Reviewers/Approvers" section of Submitter Checklist above
* [ ] When test email sent, change label to `~mktg-status::review`
* [ ] Issue requester (or reviewers) provide approval, or updates in issue comment
* [ ] Set the email to send on the agreed upon date
* [ ] Comment that the issue is scheduled for deployment (including date and time)
* [ ] Confirm that the email deployed successfully in the comments of the issue
* Close out this issue.


## Reviewer Quick Reference Notes
- Platform Chosen: ____
- [Link to send program](insert url here)
- Validation Needed? ___
- Budget Needed? If so, list costs.



<!-- These labels will be automatically applied unless you edit or delete the following section -->
/label ~"MktgOps-Priority::Top Priority" ~"dg-campaigns" ~"mktg-demandgen" ~"dg-request" ~"mktg-status::blocked"
/assign @amy.waller @nout.boctorsmith 
/cc @cbeer @jgragnola
/confidential
