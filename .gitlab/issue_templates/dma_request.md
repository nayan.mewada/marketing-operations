:exclamation: Title this issue `DMA Request - REGION - City/Area or DMA Request - Company Name`  

**Requestor**:

Please provide a summary of your intended targets for this email list:
-----

* [ ]  `Public sector`  
 
* [ ]  `Private sector`
      
    Target industries:
 
-----

### Use if Region DMA:

**Choose Region(s)** : (`AMER - West`, `AMER - East`, `AMER - PubSec`, `EMEA`, `APAC`)      
**Area to be included**: <! Please provide the geographic area that you'd like included in the DMA list> 

-----

### Use if ABM DMA:

**All relevant Account identifiers (Tier, Industry, etc.)**:   
**All relevant email domain(s)**:

-----

### Filters to consider for either list:

* Geo Target cities(s): [include all cities in the metro area]

* Geo Target state(s): [must include if including cities]

* Inclusion Job Titles (Seniority): [please note including this will significantly lessen the target list]

* Inclusion Job Titles (Function): [please note including this will significantly lessen the target list]

* Exclusion Job Titles: [i.e. HR, Finance]

* Additional inclusions:  Was sent [name of email], attended [name of event], etc

* Individuals on open opportunities? Yes/No [please note we currently can only do yes/no, not stages]

* Statuses to Suppress: Bad Data, Unqualified

* SUPPRESSIONS: Was sent [name of email], specific domains, etc

-----

### Ops Tasks  (ignore section for ABM email lists)

- [ ] Update the handbook with new DMA in appropriate `Region` &/or `Sub-Region`

-----

#### Final notes for Targeted Lists:

List request is required 7 days prior to email deployment - FMM / MPM

Final Smart List is available 2 days prior to email deployment - MOps

The list of existing Region DMAs can be viewed: https://about.gitlab.com/handbook/marketing/marketing-operations/marketo#geographic-dma-list 

/label ~"MktgOps::0 - To Be Triaged" ~"mktg-status::plan" 
