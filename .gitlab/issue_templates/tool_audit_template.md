## Please title issue `Marketing Tool Audit YYYY.MM.DD - Q#`

### Purpose

This issue is intended to be used for marketing tool user activity audits, with the end goal of determining if users still need tool access and if continued use of the tool within GitLab is warranted.

### Locations of interest
- Please make a complete copy of the [designated audit template](https://docs.google.com/spreadsheets/d/1c6GO5Q1lwrn0G9Gn_cRd87Yra_cyqCy2oG3hXzlFOaM/edit#gid=1059003918) and do not make any changes to the original spreadsheet
- Make a unique copy of the spreadsheet for each tool being audited
- Please save duplicated spreadsheets into the [designated G Suite folder](https://drive.google.com/drive/folders/1OyJWmoNbBsAsErDxiaoOqDNmfhBSa_xQ)

### Tools included on this round 
Please check the box for each tool audited this round
- [ ] Outreach 
- [ ] Zoominfo
- [ ] LinkedIn Sales Nav
- [ ] Marketo
- [ ] Sigstr
- [ ] Drift

### Results
Please include links to each spreadsheet in the comment section, with adequate labeling 

/label ~"mktg-status::plan" ~"Mktg Tool Audit" ~"MktgOps::1 - Planning"
