# User Story
<!-- Please submit issue with the following format: As a _____ (role in marketing), I would like to ____________(need), so that ________________(reason).-->
<!-- Example: As GTM management, I would like to know that all Trials are MQLing, so that MQL metrics are accurate and we can rely on them for decision making.-->

# Goal
* What do you want this to accomplish?

## Business Case


## Instructions
 - Please reference the [Zapier Connection rubric](https://about.gitlab.com/handbook/marketing/marketing-operations/zapier/#when-to-request) to determine if this request meets the criteria for a connection
 - Mops has full descretion to accept/reject this request, please allow for adequate time for planning and building.
 - Mops evaluates and triages incoming requests every Monday and that is when your request will be accepted/rejected. Please allow 2-3 weeks for Mops to build the Zap. The Zap should be set up *before* the program is set to go live.
 - Please fill in all the information below.

## Required Information   

* **Epic Link**: `Link Epic`
* **Marketo Program Link**: `Marketo URL`
* **Time Frame**:  /due `Insert Date this zap is needed`
* **Program Go-Live**: `Insert Date program is set to launch (Zap should be set up before launch)`
* **End Date**: `Date the Zap should be turned off`
* **Trigger Task**: `What will trigger this zap? (ex. New row in spreadsheet, new registration in Eventbrite, etc)`


## Requestor Tasks
These tasks need to be completed before Mops can begin work
- [ ] Access to @amy.waller or @jburton for given account (Eventbrite, Googlesheet Ownership, etc)
     - [ ] If Googlesheet, ensure the sheet has the [required fields](https://about.gitlab.com/handbook/marketing/marketing-operations/zapier/#what-data-is-required) as column headers

## Approvals
- [ ] Mops DRI agrees this meets the criteria to set up. (See Handbook for details) 


## Ops Tasks
Directions [found here](/handbook/marketing/marketing-operations/zapier/).
- [ ] Confirm you have correct access to program or sheet
- [ ] Add Account into Zapier (if not already done)
- [ ] Build Zap
- [ ] Create or Update Lead Steps (Marketo Connection)
- [ ] Map all fields
- [ ] Create List in Marketo / Confirm existing list in Marketo
- [ ] Add to Marketo List
- [ ] Update processing steps to trigger off of `Added to List`
- [ ] Testing/UAT

<!-- Do not edit below -->

/label ~"MktgOps::0 - To Be Triaged" ~"mktg-status::plan" ~Marketo
