## Rules
Please include the link to your SFDC report or Google Sheet (with sharing enabled) here: ` `

### :arrow\_right: **SLA**: Once the file has accepted, there is up to a five (5) business day turnaround time for list imports. :arrow\_left:<br>&nbsp;&nbsp;&nbsp;MktgOPS will do cursory check of list AND accept or reject list w/in 24 business hours of issue creation. 


### :pushpin: Required Fields   
- `Account ID`
- `GTM Strategy` 

### GTM Strategy Type
Select the GTM Strategy type (only 1) that you would like *all* records in your list switched to. Note: If there are different values for `GTM Strategy`, each will need to be its own request.  
* [ ] Volume
* [ ] Account Centric
* [ ] Account Based
* [ ] Acceleration

## 🛁 List clean step 1 (**`Requestor`**)

* [ ]  *Requester* - Ensure the above `Required Fields` are in the List/Report.

## ✅  List accepted by MktgOPS

* [ ]  List accepted - OPS will respond w/in 24 business hours from issue creation if list has been accepted

## 🔼 Upload list to Salesforce (MktgOPS)

* [ ]  Upload list to SFDC
* [ ]  Notify in issue when list is uploaded


### :question: Questions   
- Submit any questions about this template to the #mktgops slack channel.

/label ~"MktgOps::0 - To Be Triaged" ~"mktg-status::plan" 
/cc @rkohnke
