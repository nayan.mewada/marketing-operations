## Purpose   

Purchase of new domains (URLs) that fit our branding and company name.   

## Instructions   

1. Name this Issue - `Domain Purchase Request: <Insert Domain>` 
2. Fill in all blanks below - insufficient information will be returned &/or delay purchase  
3. Please tag your **direct** manager for approval. Work will not begin until Manager has provided approval.    

## Required Information   

| | |
| :------- | :------ | 
| Domain | `fill in URL` | 
| Business Purpose | `Add details` | 
| Time Frame Needed | Add Date |    

## Approvals   

- [ ] `**ADD Manager**`  
- [ ] MktgOPS - @sdaily


## Process - *for MktgOPS only*  

1. Check for all approvals
2. Create Instrastructure issue & tag `@dawsmith` AND apply the `~SRE On Call` label to execute the request.  
     - Infrastructure will close issue when done 
3. Once created - based on use case:  
     - Add as property in Google Console
     - Add to Google Analytics 
     - Add to Privacy Policy
     - Add to Cookiebot (requires scanning & configuration) 
4. Notify requestor & close this issue.   

/label ~"MktgOps::0 - To Be Triaged" ~"Digital Marketing Programs" ~Administrative ~"mktg-status::plan"      
/assign @darawarde  
