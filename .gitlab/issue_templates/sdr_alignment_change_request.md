## Sales Development manager instructions
1.  Please title issue using the following format: [SDR alignment change request] SDR team member name - SDR team 
2.  Provide information below as completely as possible to ensure SDR alignment is updated accurately.
3.  Add a due date to this issue that corresponds with the day you need the change to be effective. Please make an effort to open this issue as soon as you know a change needs to be made. If this issue is being opened less than two business days before the change needs to be in effect, pleast add URGENT to the beginning of the issue title. 
4.  If the change is temporary, indicate the end date of the change in the `Additional Info` section below.

## Request details

### Segment
- [ ] SMB
- [ ] Mid-Market
- [ ] Large 
- [ ] Public Sector

### Region
- [ ] APAC
- [ ] AMER
- [ ] EMEA
- [ ] LATAM

### Territory info (required)
- **High-level explanation of the change that needs to be made:**
- If `SDR Assigned` changes are required for this alignment update, please provide a Salesforce report that includes all impacted accounts. At a minimum, the report needs to include `Account ID`, `Account Owner`, `Account Name`, `SDR Assigned` and `Website` columns and be accessible to Marketing Operations. If there are other relevant fields, please include them on as a column on the report as well. <!-- Include Salesforce link here -->


### Additional info (optional)
<!-- Provide any additional information relevant to this request but not captured in the sections above. -->

## Marketing Operations tasks
*Only Marketing Operations team member should check off action items in this section.* 

Update:
- [ ] LeanData
- [ ] Salesforce
- [ ] Drift 
- [ ] Other tools as needed (include list of impacted tools as a comment on issue)

/label ~"MktgOps::0 - To Be Triaged" ~LeanData 

/assign @bethpeterson
