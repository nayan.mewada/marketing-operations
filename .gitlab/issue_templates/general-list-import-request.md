## :notepad\_spiral: List Details    

:eight\_spoked\_asterisk:  List Origin **required**:     
:eight\_spoked\_asterisk:  SFDC Campaign: `if part of a campaign or event outreach please specify`   
:eight\_spoked\_asterisk:  Marketo Campaign link: `if part of a campaign or event outreach please specify` 
:eight\_spoked\_asterisk:  Outreach Tag: `LastName - EventName`   
:eight\_spoked\_asterisk:  Google Sheet (make sure you have turned on 'sharing', if locked will delay acceptance):    

### :arrow\_right: **SLA**: Once the file has accepted, there is up to a five (5) business day turnaround time for list imports. :arrow\_left:<br>&nbsp;&nbsp;&nbsp;MktgOPS will do cursory check of list AND accept or reject list w/in 24 business hours of issue creation. 


### :pushpin: Required Fields   
- `First Name`
- `Last Name`
- `Company`
- **Origin** - This is where you recieved the list from, in EMEA only GDPR compliant sources are allowed to be imported and prospected to
- `Email` - records without an email address **will not** be imported  
- Address fields  - `Street`, `City`, `State`, etc need to be in their own individual column
- `Opt-in` - Only mark as TRUE if the prospects specifically opted into email communication. You must provide a screenshot of the language used in the issue comments.

## 🛁 List clean step 1 (**`Requestor`**)

* [ ] Select File -> Make Copy of [full spreadsheet](https://docs.google.com/spreadsheets/d/143REaMQLyIy7to-CFktL45TTTLZxBQRJUDIOMCA3CVo/edit#gid=257616838) into a new spreadsheet. Use template to copy/paste columns and alert to any email syntax errors, duplicates, and clean up Proper Capitalization. Please notify MktgOps if additional fields are needed
* [ ] Remove duplicates based on template alerts and correct errors in emails, as displayed
* [ ] Translate international lead lists as necessary - campaign owner to delegate, if relevant
* [ ] Once cleaning is completed, take cleaned data from `Right/Green` side of template and `Paste values` into the `Left/Blue` side of spreadsheet. 
 `Paste values` will paste **only data** and remove present formulas
* [ ] After cleaned data is moved to `Left/Blue` side of spreadsheet, fully delete right/green side of spreadsheet
* [ ] Rename the file to match the relevant `campaign tag`, if applicable, or campaign name 
* [ ] Link to ready-to-upload googledoc of list in issue comments, tag MktgOps (primarily @jburton)
* [ ] Add ~"List Upload: Ready" label to issue. **IMPORTANT: Reapply label for each new upload request** 
* [ ] [Change ownership of list spreadsheet](https://support.google.com/docs/answer/2494892?co=GENIE.Platform%3DDesktop&hl=en) to Marketing Ops rep performing the upload so they may erase the spreadsheet. Keep editing access for yourself

## ✅  List accepted by MktgOPS

* [ ]  List accepted - OPS will respond w/in 24 business hours from issue creation if list has been accepted

## 🔼 Upload list to Marketo (MktgOPS)

* [ ] Upload list to Marketo
* [ ] Notify in issue when list is uploaded
* [ ] Remove ~"List Upload: Ready" label
* [ ] Apply label `Lead Data: Active` if it is not already attached
* [ ] Return later to remove lead data and switch label to `Lead Data: Inactive`


### :question: Questions & FAQ  
- See [List Import](https://about.gitlab.com/handbook/marketing/marketing-operations/list-import/) section in handbook for further clarification.   
- Submit any questions about this template to the #mktgops slack channel.

/label ~"MktgOps - List Import" ~"mktg-status::plan" 
/cc @jburton
