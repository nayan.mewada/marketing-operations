## Form Overview
<!-- Link to Epic or Issue that is requesting this form -->

<!-- Please submit issue with the following format: As a _____ (role in marketing), I would like to ____________(need), so that ________________(reason).-->
<!-- Example: As GTM management, I would like to know that all Trials are MQLing, so that MQL metrics are accurate and we can rely on them for decision making.-->

### Why is this form needed? What will it be used for?

<!-- Example: I need this form to run a webcast. I will be asking for their-->
<!-- Is this a one-time use form, or global? -->
<!-- Is this form similar to any other forms we use? Please reference this [documentation for existing forms](https://docs.google.com/spreadsheets/d/1cV_hI2wAzLxYYDI-NQYF5-FDDPXPXH0VV5qRBUJAQQk/edit?usp=sharing).-->

### Do you need a new form, or update an existing form?
- [ ] New
- [ ] Existing

### Due Date
/Due 
<!-- When is the go-live date for the page? -->

### Where does/will this form live and redirect to?
- [ ] YAML
- [ ] Marketo Landing page
- [ ] External Site

<!-- Insert Landing page URL that will host this form -->


### Where does/will this form live and redirect to?
<!-- URL for the redirect -->

### Fields Requested
*Note: Mark Required fields with an asterisk* *
Reference form [guidelines](https://about.gitlab.com/handbook/marketing/marketing-operations/marketo/#forms).

The following fields must be addeed
- [x] First Name *
- [x] Last Name *
- [x] Company *
- [x] Title *
- [x] Work Email Address *
- [x] Country *
	- State/Province when United States or Canada *
	- Checkbox if from Ukraine *
- [x] Opt-in Field
- [x] Google Analytics TrackID and Client ID (Hidden)
- [x] DemandBase Fields (Hidden)
- [x] gclid (hidden)

Please add any other fields you wish to have included::
- [ ] TBD
- [ ] TBD
- [ ] TBD
<!-- add in any other fields you wish to include like Notes, subscription fields, hidden fields,  etc.-->
<!-- If you are adding a select/picklist field, please list out the values you want to be visible, and the values to store in the back end. -->

### CTA
- CTA button copy: <!-- Submit, Get in touch, etc. -->


### Other Questions:
- Do you need an embed code?
<!-- Yes or No -->

- Anything else to consider? Ex. Scoring, Automation, etc.
<!-- Insert Here -->

- What language should this form be? [Current languages available](https://about.gitlab.com/handbook/marketing/marketing-operations/marketo/#forms). 
- [ ] English
- [ ] Spanish
- [ ] French
- [ ] Germany
- [ ] Other.. `please specify`

### Tasks for Mops

- [ ] Create/Update Form
- [ ] Create New Fields (if applicable)
- [ ] [Update documentation](https://docs.google.com/spreadsheets/d/1cV_hI2wAzLxYYDI-NQYF5-FDDPXPXH0VV5qRBUJAQQk/edit#gid=951070740)
- [ ] UAT
- [ ] Go Live


<!-- Issue Labels, Do not edit under this line -->
/cc @amy.waller
/label ~"mktg-status::plan" ~"MktgOps::0 - To Be Triaged" ~"Marketo"
