**Please Note: You must have an existing Marketo form before requesting a new PathFactory form. If you need to request a new Marketo form, use the `form_request.md` issue template in the marketing ops project then link that issue to this one.**

The purpose of this issue template is to request a new PathFactory form to use in form strategy on content tracks. Please fill in all the deatils about your request to get the best assistance.

### Requester (you)

- [ ] Name this issue: `PathFactory Form - [Name of Marketo Form] - [Marketo Form ID]`
- [ ] Link to existing issue for Marketo form in [related issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html)
- [ ] Enter a due date if applicable and apply to the issue
- [ ] Link to Marketo form: 
- [ ] Approve form 

### MktgOps

- [ ] Add capture tags and Google Tag Manager script to Marketo form embed code
- [ ] Add form script to PathFactory as `[WIP]`
- [ ] Request and confirm QA review of PF form script
- [ ] Paste preview in issue and ping form requester for approval 
- [ ] Mark form in PF as `[LIVE]` and close issue

/label ~"MktgOps::1 - Planning" ~"PathFactory" 
/assign @sdaily
