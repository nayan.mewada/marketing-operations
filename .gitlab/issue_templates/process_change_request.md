This template is for requesting a new process or a change to an existing process related to a Marketing Operations function or tool. The more this request is filled out, the quicker and better the Marketing Operations team can help and solution.

#### Describe the current process in as much detail as you can provide with any relevant links

<!-- Ex. Currently, we have to manually edit the spreadsheet with information from this System A (link) in order to upload to this system which increases time and decreases productivity.-->

`Insert answer here`

#### What is your job / role

<!-- Please add any job role relevant information about yourself here pertaining to the current process. Ex. I am campaign manager and the current process is difficult because x, y, z.-->

`Insert answer here`

#### What system is affected by this issue?
<!-- Select all that apply -->

- [ ] Salesforce.com
- [ ] LeanData
- [ ] Outreach
- [ ] Marketo
- [ ] Sales Insight / Interesting Moments
- [ ] Drift
- [ ] PathFactory
- [ ] Bizible
- [ ] Smartling
- [ ] Terminus Email Experiences
- [ ] ZoomInfo
- [ ] Linkedin Sales Navigator
- [ ] Other `Insert here`

#### Please provide any relevant examples 

<!--  Insert IDs or URLs, if sending email addresses or PII, this issue must be confidential -->

`Insert Examples Here`

#### Please provide any relevant screenshots or screencasts

<!-- How to take a screenshot: https://www.howtogeek.com/205375/how-to-take-screenshots-on-almost-any-device/ -->

`Insert screenshot here`

#### What is/are the relevant URL(s)

`Insert full URL(s) here`

<!-- These labels will be automatically applied unless you edit or delete the following section -->
/label ~"MktgOps::0 - To Be Triaged" 
