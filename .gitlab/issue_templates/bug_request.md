This template is for filing a bug report related to a Marketing Operations function or tool. The more this request is filled out, the quicker and better the Marketing Operations team can help and solution.

#### Briefly describe the bug :bug: 
<!-- Ex. A group of leads were reassigned yesterday when they MQLd, is this intended Leandata behavior? -->

`Insert answer here`

#### If possible, please specify how to reproduce the bug :bug: 

`Insert answer here`

#### Urgency :exclamation: 

Please select the most accurate priority label. Typical level is P2, if there is a workaround available, do not go higher to P1 or P0. 

- [ ] PO Critical - (System down, core business function down, or potential loss of mission critical data. Timeframe: Immediate action needed. Pull into active milestone.) Tag MktgOps team. Use label ~"MktgOps-Urgency::P0"
- [ ] P1 Urgent - (Major feature or workflow is not working. Timeframe: Action within days. Pull into active milestone.) Use label ~"MktgOps-Urgency::P1"
- [ ] P2 Important - (Requires attention, but normal workflow is not impacted or there is a workaround. Timeframe: Action within next milestone.) Use label ~"MktgOps-Urgency::P2"
- [ ] P3 Minor - (Requires attention, but normal workflow is not impacted. Minor, low urgency. Timeframe: Future action needed, not priority or urgent.) Use label ~"MktgOps-Urgency::P3" 
- [ ] P4 Low - (Small bug, nice to have fixed, but not impacting workflow. Timeframe: Future action needed, not priority or urgent) Use label ~"MktgOps-Urgency::P4"

#### What is your job / role

<!-- Please add any job role relevant information about yourself here pertaining to the bug. Ex. I am campaign manager and I am unable to clone an asset in Marketo. -->

`Insert answer here`

#### What process is being affected?
<!-- Select all that apply -->

- [ ] Lead Routing / Assignment
- [ ] Lead/Contact Details
- [ ] List Loads
- [ ] Sync Errors / Issues
- [ ] Email lists
- [ ] Email Programs
- [ ] Program/Campaign issues
- [ ] Interesting Moments / Sales Insight
- [ ] Scoring discrepency
- [ ] Other `Insert here`

#### What system is affected by this issue?
<!-- Select all that apply -->

- [ ] Salesforce.com
- [ ] LeanData
- [ ] Outreach
- [ ] Marketo
- [ ] Sales Insight / Interesting Moments
- [ ] Drift
- [ ] PathFactory
- [ ] Bizible
- [ ] Smartling
- [ ] Terminus Email Experiences
- [ ] ZoomInfo
- [ ] Linkedin Sales Navigator
- [ ] Other `Insert here`


#### Please provide any relevant examples 

<!--  Insert IDs or URLs, if sending email addresses or PII, this issue must be confidential -->

`Insert Examples Here`

#### Please provide any relevant screenshots or screencasts

<!-- How to take a screenshot: https://www.howtogeek.com/205375/how-to-take-screenshots-on-almost-any-device/ -->

`Insert screenshot here`

#### What is/are the relevant URL(s)

`Insert full URL(s) here`



<!-- These labels will be automatically applied unless you edit or delete the following section -->
/label ~"MktgOps::0 - To Be Triaged" ~"MktgOps - bug"
