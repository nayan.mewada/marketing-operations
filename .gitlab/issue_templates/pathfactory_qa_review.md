The purpose of this issue template is to request a quality assurance review for a PathFactory (PF) experience (target track, recommended, track, website promoter, explore page, etc.) Please fill in all the deatils about your request to get the best assistance. 

### Requester (you)

- [ ] Name this issue: `PathFactory QA Review - [Name of PF Experience]`
- [ ] Link to existing issue for this PF experience
- [ ] Enter a due date if relevant

### MOps QA Review

_A checkmark represents you either checked it and it's no applicable, or you checked and it's applicable and correct._

- [ ] All gated content has form strategy enabled
- [ ] If form strategy enabled, `Show to known users` is unchecked
- [ ] `Search Engine Directive` is set to `No Index, No Follow`
- [ ] Asset titles in the track have no `| GitLab` 
- [ ] `Cookie Consent` is turned on
- [ ] `External ID` is set to default

/label ~"MktgOps::1 - Planning" ~"PathFactory" ~"mktg-status::plan" 
/assign @sdaily
